
// Todo: build objects from the functions

// todo: instead of adding vars to strings, create html elements and set their .html()


function getCurrentQuestionaireTitle() {
    return $('.questionaire-choices').val();
}


function updateNotificationBadges() {

    let countNewAnchors = {
        all: 0,
        danger: 0,
        info: 0,
        success: 0
    };

    $('.anchor-popup-text').each(function () {

        if ($(this).hasClass('alert-info')) {
            countNewAnchors.info++;
        }
        if ($(this).hasClass('alert-danger')) {
            countNewAnchors.danger++;
        }
        if ($(this).hasClass('alert-success')) {
            countNewAnchors.success++;
        }

    });


    // update notification badge
    if (countNewAnchors.info > 0) {
        $('.anchor-color-button.info').children('.button-badge').html(countNewAnchors.info).show();
    } else {
        $('.anchor-color-button.info').children('.button-badge').html(countNewAnchors.info).hide();
    }

    if (countNewAnchors.danger > 0) {
        $('.anchor-color-button.danger').children('.button-badge').html(countNewAnchors.danger).show();
    } else {
        $('.anchor-color-button.danger').children('.button-badge').html(countNewAnchors.danger).hide();
    }

    if (countNewAnchors.success > 0) {
        $('.anchor-color-button.success').children('.button-badge').html(countNewAnchors.success).show();
    } else {
        $('.anchor-color-button.success').children('.button-badge').html(countNewAnchors.success).hide();
    }
}

function modifyAnchorsForPrint() {
    $('.anchor-popup-nav ').toggle();
}


function refreshPage() {

    $('#intro-description').slideUp().fadeOut(1000);
    $('.intro-banner-made-by').slideUp().fadeOut(1000);
    $('#fs-form-wrap').slideUp().fadeIn(1000);
    $('#limit-search-box').slideUp().fadeIn(1000);

    // update open cases in nav
    $.post($('#nav-cases').data('url'),
        {
            questionaire: getCurrentQuestionaireTitle(),
        },
        function (data, status) {
            let dataCases = jQuery.parseJSON(data);
            if (parseInt(dataCases.caseCount) !== 0) {
                $('#nav-cases').find('.button-badge').show();
                $('#nav-cases').find('.button-badge').html(dataCases.caseCount);

            } else {
                $('#nav-cases').find('.button-badge').hide();
            }
            $('#nav-cases').find('a').attr("href", dataCases.url);

        });

    // // remove all anchor popups
    $("#anchor-popup").empty();

    // update badges
    updateNotificationBadges();
}

let questionaireResultSlug = '';


// implement expand / collapse context
function generateAnchorExpandedText(text, context) {
    // default
    let anchorTextParts = {
        text: text,
        context: context,
        expandTextBefore: '... ',
        expandTextAfter: ' ...',
        modalText: text,
    };

    // if context is set, update the properties
    if (context !== null) {

        // get text before and after the relevant quote
        anchorTextParts.text += ' ... <strong class=""> (more)</strong>';
        anchorTextParts.modalText = context;

    }
    return anchorTextParts;
}

// generate the source structure
function generateAnchorSource(title, publishedAt) {
    return title + ', ' + new Date(publishedAt).getFullYear();
}

function getAnchorAlertClass(anchor) {
    let bootstrapClassAlertBox = 'alert-info';
    if (anchor.algorithm == null) {
        bootstrapClassAlertBox = 'alert-info';
    } else {
        if (anchor.algorithm.choseRecommendedPath === "false") {

            bootstrapClassAlertBox = 'alert-danger';
        }
        if (anchor.algorithm.choseRecommendedPath === "true") {
            bootstrapClassAlertBox = 'alert-success';
        }

        if (anchor.algorithm.choseRecommendedPath === "") {
            bootstrapClassAlertBox = 'alert-info';
        }

    }

    return bootstrapClassAlertBox;
}

function generateAnchorDescription(description) {
    return '<div class="anchor-popup-title"><p><strong>' + description + '</strong></p></div>';
}

function generateAnchorText(anchor) {

    let anchorTextParts = generateAnchorExpandedText(anchor.text, anchor.context);

    let anchorSource = generateAnchorSource(anchor.guideline.title, anchor.guideline.publishedAt);

    return '<div class="anchor-popup-text table-responsive alert ' + getAnchorAlertClass(anchor) + '" data-anchor-id="' + anchor.id + '">' +
        '<div class="anchor-popup-nav text-right"><span class="understand-anchor-popup tooltip-frontend" title="Why do I see this quote?"><i class="fa fa-question"></i></span>' +
        '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="promote-anchor-popup tooltip-frontend"  title="That\'s important! Please highlight for later use."><i class="fa fa-heart"></i>' +
        '</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="close-anchor-popup tooltip-frontend"  title="Not useful"> <i class="fa fa-remove " ></i></span></div>' +
        '<div class="anchor-popup-quote">"' +anchorTextParts.text + '</div>'+
        // anchorTextParts.expandTextBefore +  + anchorTextParts.expandTextAfter + '" <strong style="display:none;" class="js-toggle-context-anchor"> (reduce)</strong>' +
        '<div class="anchor-popup-source text-right tooltip-frontend" title="' + anchorSource + '">Source</div>' +
        '</div>';
}

function generateAnchorBox(dataValue) {
    return '<div ' + dataValue + ' class="col-md-12 anchor-box table-responsive alert alert-dark"></div>';
}


function generateModal(classes, dataAttribute, title, body) {
    return '                                <div class="modal fade ' + classes + '" ' + dataAttribute + ' tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">\n' +
        '                                        <div class="modal-dialog modal-lg" role="document">\n' +
        '                                        <div class="modal-content">\n' +
        '                                        <div class="modal-header">\n' +
        '                                    <h5 class="modal-title">' + title + '</h5>' +
        '                                    </div>\n' +
        '                                    <div class="modal-body">\n' +
        '                                <p> ' + body + ' </p>\n' +
        '                                </div>\n' +
        '                                    <div class="modal-footer">\n' +
        '                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>\n' +
        '                                    </div>\n' +
        '                                    </div>\n' +
        '                                    </div>\n' +
        '                                    </div>';
}


function generateAnchorModal(anchor) {

    // check whether modal already exists
    if (!$('.anchor-modal[data-anchor-id=' + anchor.id + ']').length) {

        // create new modal

        let anchorTextParts = generateAnchorExpandedText(anchor.text, anchor.context);

        // add a modal to body
        modal = generateModal('anchor-modal', 'data-anchor-id="' + anchor.id + '"', generateAnchorSource(anchor.guideline.title, anchor.guideline.publishedAt), anchorTextParts.modalText);
        $(modal).hide();
        $("body").append(modal);
    }


}


// use the data from the API to add anchors to the page
function addAnchors(anchorsJSON) {

    // count the new anchors
    let countNewAnchors = {
        all: 0,
    };

    $.each(anchorsJSON, function (key, anchor) {

        // determine algorithm or questionItem anchor

        if (anchor.questionItem == null) {
            // type algorithm

            let anchorBox = $('.anchor-box[data-algorithm-id=' + anchor.algorithm.id + ']');

            // if algorithm anchor popup box already exists append, else create new one
            if (anchorBox.length) {

                // checke whether this anchor was already added before
                if (!$(anchorBox).children('[data-anchor-id="' + anchor.id + '"]')[0]) {

                    // reformat the anchor box by making it bigger and giving it a dark background
                    anchorBox.removeClass('col-md-6').addClass('col-md-12');

                    //append
                    anchorBox.append(generateAnchorText(anchor));

                    // create modal
                    generateAnchorModal(anchor);


                    //count new anchors
                    countNewAnchors.all++;
                }

            } else {
                // create box
                $("#anchor-popup").prepend(generateAnchorBox('data-algorithm-id="' + anchor.algorithm.id + '"')).hide().fadeIn(1000);

                // append description and text
                $('.anchor-box[data-algorithm-id=' + anchor.algorithm.id + ']').append(generateAnchorDescription(anchor.algorithm.description)).append(generateAnchorText(anchor));

                // create modal
                generateAnchorModal(anchor);

                // count new anchors
                countNewAnchors.all++;
            }

        } else {
            // type questionItem

            if (!$('.anchor-box[data-questionitem-id=' + anchor.questionItem.id + ']').length) {
                $("#anchor-popup").prepend(generateAnchorBox('data-questionitem-id="' + anchor.questionItem.id + '"')).hide().fadeIn(1000);

                // append description
                $('.anchor-box[data-questionitem-id=' + anchor.questionItem.id + ']').append(generateAnchorDescription(''));


                //append
                $('.anchor-box[data-questionitem-id=' + anchor.questionItem.id + ']').append(generateAnchorText(anchor));

                // create modal
                generateAnchorModal(anchor);


                // count new anchors
                countNewAnchors.all++;
            }

        }

        // update bootstrap info box classes
        $('.anchor-popup-text[data-anchor-id="' + anchor.id + '"]').removeClass('alert-info alert-danger alert-success').addClass(getAnchorAlertClass(anchor));

    });


    // add a notification about the new quotes
    if (countNewAnchors.all > 0) {
        addNotification('<strong>' + countNewAnchors.all + '</strong> quote(s) added.', 'info');
    }
}


// function to loop over all anchors and remove those that are invalid now
function removeInvalidAnchors(selectedQuestionItemIds, activeAlgorithms) {
    // loop over all popups
    $('.anchor-box').each(function () {

        // check whether anchor is from question-item or algorithm
        if ($(this).data('questionitem-id')) {

            // remove anchor if questionItem of anchor is not selected
            if (!selectedQuestionItemIds.includes($(this).data('questionitem-id')) && !$(this).is(":hidden")) {

                $(this).fadeOut(1000);
                $(this).remove();

                // same for the modal
                $('#anchor-modal' + $(this).data('anchor-id')).remove();
            }
        }

        if ($(this).data('algorithm-id')) {

            // remove anchor if anchor's algorithm is not active anymore.
            if (!activeAlgorithms.includes($(this).data('algorithm-id')) && !$(this).is(":hidden")) {
                $(this).fadeOut(1000);
                $(this).remove();

                // same for the modal
                $('#anchor-modal' + $(this).data('anchor-id')).remove();
            }
        }


    });
}


// sort anchor popups by danger first, success second and info last
function sortAnchorPopups() {
    $('#anchor-popup').find('.anchor-popup-text').each(function () {
        if ($(this).hasClass('alert-danger')) {
            $('#anchor-popup').prepend($(this).closest('.anchor-box'));
        }
        if ($(this).hasClass('alert-success')) {
        }
        if ($(this).hasClass('alert-info')) {
            $('#anchor-popup').append($(this).closest('.anchor-box'));
        }
    });
}

// Function to convert form to array for saving questionaire
$.fn.getFormObject = function () {
    var object = $(this).serializeArray().reduce(function (obj, item) {
        var name = item.name.replace("[]", "");
        if (typeof obj[name] !== "undefined") {
            if (!Array.isArray(obj[name])) {
                obj[name] = [obj[name], item.value];
            } else {
                obj[name].push(item.value);
            }
        } else {
            obj[name] = item.value;
        }
        return obj;
    }, {});
    return object;
}

// Function ends


function getAllCurrentAnchorIds() {
    let anchorIds = [];
    $('#anchor-popup').find('.anchor-popup-text').each(function () {
        anchorIds.push($(this).data('anchor-id'));
    });

    return anchorIds;
}

function saveQuestionaire(flag) {
    let serializedQuestionaire = $('#questionaire').getFormObject();

    $.post($('#questionaire').data('url-submit'),
        {
            slug: questionaireResultSlug,
            questionaireName: getCurrentQuestionaireTitle(),
            questionItems: serializedQuestionaire,
            anchorIds: getAllCurrentAnchorIds(),
            flag: flag,
        },
        function (data, status) {
        });
}

// update page for submit
function updatePageForSubmit() {
    // TODO Send an ajax call to submit questionaire data
    saveQuestionaire();

    // hide limit search box
    $('#limit-search-box').hide();

    // reset background gradient
    $('body').css('background-image', 'initial');


    sortAnchorPopups();

    // notification about sorting the popups
    addNotification('Your Guideline excerpts have been ordered', 'info');

    // remove scroll bar from anchor popups
    $('#anchor-popup').removeClass('pre-scrollable');

    // hide all previous fields of questionaire
    $('.js-questionaire-item').hide();
    $('.fs-form-overview .fs-fields > li, .no-js .fs-form .fs-fields > li').css('border-bottom', 'none');
    $('.fs-form-overview .fs-fields > li, .no-js .fs-form .fs-fields > li').css('margin', 'unset');
    $('.fs-form-overview .fs-fields > li, .no-js .fs-form .fs-fields > li').css('padding', 'unset ');


    // add fields for creating a user account
    formItems = '                                <li>\n' +
        '                                    <label class=""\n' +
        '                                           ><p>FungAssist provided you with all the relevant guideline excerpts for your case (<a class="js-download-anchor-popups">download a pdf here! <span class="glyphicon glyphicon-file"></span></a>). </p><p>But there is more: You can publish your case on FungAssist. This allows selected experts to <strong>support you</strong> with your case. For patient\'s privacy we only ask you for a passphrase ' +
        'for later authentication. Cases will be closed after 4 weeks.</p></label>\n' +
        '                                </li>';

    formItems += '                                <li>\n' +
        '                                    <label class=""\n' +
        '                                           >Your most important question about your case</label>\n' +
        '                                    <input type="text" placeholder="e.g. What is the best treatment in this particular case?" name="question">\n' +
        '                                </li>';

    formItems += '                                <li>\n' +
        '                                    <label class=""\n' +
        '                                           >Your Passphrase</label>\n' +
        '                                    <input type="password" placeholder="random words are great passwords" name="password">\n' +
        '                                </li>';

    // formItems += '                                <li>\n' +
    //     '                                    <label class=""\n' +
    //     '                                           >Repeat Passphrase</label>\n' +
    //     '                                    <input type="password" placeholder="random words are still great passwords" name="password-check">\n' +
    //     '                                </li>';
    $('#questionaire > .fs-fields').append(formItems);


}

function getPatientReport() {

    return $.post($('#questionaire').data('url-patient-report'),
        {
            slug: questionaireResultSlug,
        },
        function (data, status) {
            return data;
        });


}

$(document).ready(function () {

    $('.intro').fadeIn(1500);

    $('.navbar').delay(5000).fadeIn(2000);

    $('.questionaire-choices').on('change', function () {

        // get and set questionaireResultSlug
        $.post($('#questionaire').data('url-slug'),
            {
                questionaireTitle: getCurrentQuestionaireTitle(),
            },
            function (data, status) {
                questionaireResultSlug = data;
            });

        refreshPage();

    });

    // add fancy tooltip layout
    new jBox('Tooltip', {
        attach: '.tooltip-frontend',
        // trigger: 'click',
    });


    // manage disclaimer
    if (Cookies.get('disclaimer-fungassist') === 'agree') {

    } else {
        $('#disclaimer-modal').modal({backdrop: 'static', keyboard: false})
    }

    $('#btn-save-disclaimer').on('click', function () {
        if ($("#agree-to").is(':checked') && $("#agree-to-2").is(':checked')) {
            $('#disclaimer-modal').modal('hide');
        }

        // save that
        Cookies.set('disclaimer-fungassist', 'agree')
    });


    // animate the homepage for nicer look: add sidenav and resize questionaire
    $('body').on('click', '#questionaire', function () {

        $('#questionaire-wrapper').removeClass('col-md-offset-3', 1000);
        setTimeout(
            function () {
                $('#sidenav').fadeIn(1000);
                $('#intro-description').fadeOut(1000);
            }, 1000);

    });


    // enlarge thumbnail images
    $('body').on('click', '.img-thumbnail', function () {
        var src = $(this).attr('src');
        $('<div>').css({
            background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
            backgroundSize: 'contain',
            width: '100%', height: '100%',
            position: 'fixed',
            zIndex: '10000',
            top: '0', left: '0',
            cursor: 'zoom-out'
        }).click(function () {
            $(this).remove();
        }).appendTo('body');
    });


    // hide 'not important' anchors by click from user
    $('body').on('click', '.close-anchor-popup', function () {
        $(this).closest('.anchor-popup-text').hide();
    });

    // print anchor popups
    $('body').on('click', '.js-download-anchor-popups', function () {


        $.when(getPatientReport()).done(function (response) {
            modifyAnchorsForPrint();

            let patientReport = '';
            patientReport = '<h1>Your case: </h1><div>' + response + '</div>';
            var divContents = $("#anchor-popup").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Your Patient FungAssist Case</title>');
            printWindow.document.write('<link rel="stylesheet" href="css/print.css" />');
            printWindow.document.write('</head><body >');
            printWindow.document.write(patientReport);
            printWindow.document.write('<br><br><br><br>');
            printWindow.document.write('Your case\' FungAssist ID: ' + questionaireResultSlug);
            printWindow.document.write('<br><br><br><br>');
            printWindow.document.write('Your guideline excerpts: ' + divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();

            setTimeout(
                function () {
                    printWindow.print();

                }, 1000);
            modifyAnchorsForPrint();
        });


    });

    // hide 'not important' anchors by click from user
    $('body').on('click', '.close-anchor-box', function () {
        $(this).closest('.anchor-box').hide();
    });


    // highlight useful comments
    $('body').on('click', '.promote-anchor-popup', function () {
        if ($(this).css('color').replace(/\s+/g, '') === 'rgb(255,92,51)') {
            $(this).css('color', 'unset');
        } else {
            $(this).css('color', 'rgb(255,92,51)');
        }
    });


    // expand/collapse anchor context
    $('body').on('click', '.js-toggle-context-anchor', function (e) {

        e.stopPropagation();
        $(this).closest('.anchor-popup-text').find(".js-context-anchor").toggle();
        $(this).closest('.anchor-popup-text').find('.js-toggle-context-anchor').toggle();
        // $(this).hide();

    });

    // open a modal to maximize the content of a popup
    $('body').on('click', '.anchor-popup-quote', function () {
        anchorPopupId = $(this).closest('.anchor-popup-text').attr('data-anchor-id');
        anchorId = anchorPopupId.replace(/^\D+/g, '');
        $('.anchor-modal[data-anchor-id="' + anchorId + '"]').modal();


    });

    // hide / show specific types of anchors
    $('.anchor-color-button').on('click', function () {
        if ($(this).hasClass('info')) {
            $('.anchor-popup-text.alert-danger').closest('.anchor-box').toggle();
            $('.anchor-popup-text.alert-success').closest('.anchor-box').toggle();
        }
        if ($(this).hasClass('danger')) {
            $('.anchor-popup-text.alert-info').closest('.anchor-box').toggle();
            $('.anchor-popup-text.alert-success').closest('.anchor-box').toggle();
        }
        if ($(this).hasClass('success')) {
            $('.anchor-popup-text.alert-danger').closest('.anchor-box').toggle();
            $('.anchor-popup-text.alert-info').closest('.anchor-box').toggle();
        }

    });

    $("#questionaire").submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        $.when(saveQuestionaire('finalSubmit')).done(function () {
            window.location.replace(url);


        });


    });

    // guideline search toggle
    $('.limit-search-lable').on('click', function () {

        if (!$('#limit-search-selectbox').hasClass('chosen-select')) {
            $('#limit-search-selectbox').toggle();
            $('#limit-search-selectbox').addClass('chosen-select');
            $(".chosen-select").chosen();
        } else {
            $('#limit-search-box').find('.chosen-container').toggle();

        }

    });

    $('option').mousedown(function(e) {
        e.preventDefault();
        $(this).prop('selected', !$(this).prop('selected'));
        return false;
    });

});

