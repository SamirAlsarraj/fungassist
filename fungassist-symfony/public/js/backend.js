function validateJsSortable() {
    let errorMessages = [];

    // is empty
    if ($('.list-group-item').length < 2) {
        errorMessages.push('You need at least 2 elements');
    }

    // max depth 3
    if($('.list-group-item').last().parents(".list-group-item").length > 2){
        errorMessages.push('A maximum of 3 levels is allowed');
    }

    // needs to start with operator
    if ($('.list-group-item').first().data('type') !== 'operator') {
        $('.list-group-item').first().effect('highlight', {}, 3000);
        errorMessages.push('You have to start with an operator!');
    }

    // only one element at the top of the tree
    if ($('#sortableIfStatement').children().length > 1) {
        $('#sortableIfStatement').children().effect('highlight', {}, 3000);
        errorMessages.push('Only one operator item at the top of the logic tree!');
    }

    $('.list-group-item').each(function () {
        // only operators can have children
        if ($(this).find('.list-group-item').length > 0 && $(this).data('type') !== 'operator') {
            $(this).effect('highlight', {}, 3000);
            errorMessages.push('Only operators are allowed to have children!');
        }

        // and operators have to have children
        if ($(this).find('.list-group-item').length === 0 && $(this).data('type') === 'operator') {
            $(this).effect('highlight', {}, 3000);
            errorMessages.push('Operator items need to have children!');
        }
    });


    return errorMessages;
}


function printJsSortableErrorMessages() {

    let errorMessages = validateJsSortable();

    errorMessages.forEach(function (message) {
        addNotification(message, 'danger');
    })

}


// update sortable If Statements
function updateSortableJSList() {

    // get all sotrable Items
    var nestedSortables = [].slice.call(document.querySelectorAll('.nested-sortable'));

    // Loop through each nested sortable element
    for (var i = 0; i < nestedSortables.length; i++) {
        new Sortable(nestedSortables[i], {
            group: 'nested',
            animation: 150,
            fallbackOnBody: true,
            swapThreshold: 0.65,
        });
    }
}

function getListItemElementAsJSON(element) {
    switch ($(element).data('type')) {
        case 'algorithm':
            // depending on whether number is positive or negative
            if (parseInt($(element).data('id')) > 0) {
                return {"==": [{"algorithm": parseInt($(element).data('id'))}, true]}

            } else {
                return {"==": [{"algorithm": Math.abs(parseInt($(element).data('id')))}, false]}
            }
        case 'questionItem':
            // depending on whether number is positive or negative
            if (parseInt($(element).data('id')) > 0) {
                return {"==": [{"var": parseInt($(element).data('id'))}, true]}

            } else {
                return {"==": [{"var": Math.abs(parseInt($(element).data('id')))}, false]}
            }
        default:
            return false;
        // code block
    }
}


// creates a JSON element that is useable for JSONLogic
function jsSortableToJSONLogic() {


    // Loop through each nested sortable element
    let itemTree = {};
    $('#sortableIfStatement').children('.list-group-item').each(function () {
        // build first tree element
        let firstLevelOperator = $(this).data('id');
        itemTree = {
            [firstLevelOperator]: [],
        };

        // get children from the next empty-sortable box
        $(this).children('.empty-sortable').children('.list-group-item').each(function () {

            // get children of children if they exist
            let childrensChild = [];
            if ($(this).data('type') === 'operator') {
                // get children

                $(this).children('.empty-sortable').children('.list-group-item').each(function () {
                    childrensChild.push(getListItemElementAsJSON($(this)));
                });

                let secondLevelOperator = $(this).data('id');
                itemTree[firstLevelOperator].push({
                    [secondLevelOperator]: childrensChild,
                });


            } else {
                itemTree[firstLevelOperator].push(getListItemElementAsJSON($(this)));
            }
        });


    });

    // build final string
    return itemTree;
}

function jsSortableStarsToJSON() {
    // get the recommendedPath
    let recommendedPathQuestionItemIds = [];

    $('.recommendedPath').each(function () {
        recommendedPathQuestionItemIds.push($(this).data('id'));
    });

    let finalStringRecommendedPath = '';
    if (recommendedPathQuestionItemIds.length > 0) {
        finalStringRecommendedPath = '{"and" : [' + getVarsAsString(recommendedPathQuestionItemIds) + ']}';
    }

    return finalStringRecommendedPath;
}


$(document).ready(function () {

    // sort a specific droptdown field
    sortDropdownByClass('js-select-questionitem');

    // send the reject message to the server and reload the current window
    $('.reject-anchor-submit').click(function () {
        textarea = '#message-text-anchor-' + $(this).val();
        rejectText = $(textarea).val();

        $.post($(this).data('reject-url'),
            {
                rejectText: rejectText,
                anchorId: $(this).val(),

            },
            function (data, status, xhr) {
            });
        location.reload();

    });

    // reset form fields
    $('body').on('click', '.js-form-field-remove-content', function () {
        console.log($(this).data('form-field-id'));
        $('#' + $(this).data('form-field-id')).val('');
    });


    /**
     *
     * Sortable If Statement Stuff
     *
     */

        // template for html elements for sortable list items
    const sortableListItemTemplate = (element) => `<div class="list-group-item" data-id="${element.id}" data-type="${element.type}">${element.text}&nbsp;&nbsp;&nbsp;
            <span class="glyphicon glyphicon-trash js-remove-sortable-item"></span>${element.additionalText}
            <div class="empty-sortable list-group nested-sortable"></div>
            </div>`;


    // add operator, questionItem and algorithm item to sortable If statement
    $(".js-select-operator,.js-select-questionitem,.js-select-algorithm").chosen().change(function(){

        // get selected item and use preset template variable
        currentOption = $(this).find("option:selected");
        let element = {
            id: currentOption.val(),
            text: currentOption.text(),
            type: currentOption.data('type'),
            additionalText: '',
        };

        // add more functions for non operator-type items
        if (currentOption.data('type') !== 'operator') {
            element.additionalText = `&nbsp;&nbsp;&nbsp;<span title="Mark as recommended Path" class="glyphicon glyphicon-star js-mark-recommendedPath"></span>
            &nbsp;&nbsp;&nbsp;<span title="Treat this item as false" class="glyphicon glyphicon-flash js-falsify-questionItem"></span>`;
        }

        // append element
        $('#sortableIfStatement').append(sortableListItemTemplate(element));

        // update sortable list
        updateSortableJSList();
    });


    // implement remove by clicking on trash icon
    $('body').on('click', '.js-remove-sortable-item', function () {
        $(this).closest('.list-group-item').remove();
    });

    // implement marking recommendedPath Icons
    $('body').on('click', '.js-mark-recommendedPath', function () {
        $(this).closest('.list-group-item').toggleClass('recommendedPath');
    });

    // implement false questionItem
    $('body').on('click', '.js-falsify-questionItem', function () {
        currentDataId = $(this).closest('.list-group-item').attr('data-id') * (-1);
        $(this).closest('.list-group-item').attr('data-id', currentDataId);
        if ($(this).closest('.list-group-item').attr('data-id') < 0) {
            $(this).css('color', 'red');
        } else {
            $(this).css('color', 'black');
        }
    });


    // convert sortable If Statement List to JSON for JSON Logic, update the form and submit it
    $('#js-sortable-to-json').on('click', function () {

        let finalStringIfStatement = JSON.stringify(jsSortableToJSONLogic());

        // set the form's ifStatement to our convertet sortable Ifstatement and submit form
        $('#' + $(this).data('form-item-ifstatement')).val(finalStringIfStatement);

        // in case there is a field for stared items
        if ($(this).data('form-item-star') !== '') {
            let finalStringRecommendedPath = jsSortableStarsToJSON();
            $('#' + $(this).data('form-item-star')).val(finalStringRecommendedPath);
        }

        // check for error messages :)
        if (validateJsSortable().length === 0) {
            $('form[name=' + $(this).data('form') + ']').trigger('submit');
        } else {
            printJsSortableErrorMessages();
        }


    });

});