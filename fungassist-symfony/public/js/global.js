function sortDropdownByClass(classNameSelectItem){
    // sort options of a certain dropdown class
    var options = $('.'+classNameSelectItem+' option');
    var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
    arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
    options.each(function(i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
    });
}

function isEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

function reloadTooltips(){
    // load new tootips
    new jBox('Tooltip', {
        attach: '.tooltip-frontend',
        // trigger: 'click',
    });
}


$(document).ready(function () {

    // make chosen select boxes available
    $(".chosen-select").chosen();

    // navigation dropdown
    $('.dropdown-toggle').dropdown();


    /**
     *
     * Manage Feedback
     *
     */

    // open modal for feedback
    $('#feedback-button').click(function () {
        $('#feedback-modal').modal();
    });

    // submit feedback via ajax to the server and create a github issue by sending it via email to our service center
    $('#feedback-send').on('click', function () {
        event.preventDefault();
        let errors = 0;

        // check for empty feedback-text
        if($('#feedback-message').val().length < 5){
            $('#feedback-message').closest('.form-group').addClass('has-error tooltip-frontend').attr('title','You have to write some text :)');
            reloadTooltips();
            errors++;
        } else{
            $('#feedback-message').closest('.form-group').removeClass('has-error').addClass('has-success').attr('title','');
        }

        // check for correct email if something was set
        if(!isEmail($('#feedback-replyTo').val()) && $('#feedback-replyTo').val().length > 0){
            $('#feedback-replyTo').closest('.form-group').addClass('has-error tooltip-frontend').attr('title','You have to provide a valid email.');
            reloadTooltips();
            errors++;
        } else{
            $('#feedback-replyTo').closest('.form-group').addClass('has-success').removeClass('has-error').attr('title','');
        }


        if(errors === 0){

            // ajax request to server
            $.post($(this).data('url'),
                {
                    feedbackText: $('#feedback-message').val(),
                    feedbackReplyTo:  $('#feedback-replyTo').val(),

                },
                function (data, status) {
                console.log(data);
                    if (status === 'success') {

                        // hide the modal
                        $('#feedback-modal').modal('hide');

                        // reset the fields
                        $('#feedback-replyTo').val('');
                        $('#feedback-message').val('');

                        // thank you message in the navigation bar
                        $('#feedback-button').html('<a><span class="glyphicon glyphicon-thumbs-up"></span> Thank you!</a>');
                        setTimeout(
                            function () {
                                $('#feedback-button').html('<a><span class="glyphicon glyphicon-envelope"></span> Feedback</a>');
                            }, 5000);

                    } else {
                        alert('something went wrong :/ Please send an email to maximilian.schons@uk-koeln.de. Thanks!');
                    }
                });

        }

    });

});

function addNotification(message, type) {
    $.notify({
        // options
        message: message
    }, {
        // settings
        type: type,
        delay: 2000,
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}