<?php
require 'vendor/autoload.php';


/**
 *
 * How-to:
 * On the terminal go to the directory of this file and execute it using `php api-testing.php`
 *
 */




$client = new \GuzzleHttp\Client();
$baseURL = 'http://fungassist.local/api/v1/';




/**
 * examples
 */
//// Provide the body as a string.
//$response = $client->request('POST', $baseURL.'getReadableAlgorithmText', [
//    'form_params' => [
//        'algorithmId' => 1,
//    ]
//
//]);

//
//// easy get request
//$response = $client->request('GET', $baseURL.'XXX');

echo $response->getStatusCode();
echo $response->getHeaderLine('content-type');
echo $response->getBody();