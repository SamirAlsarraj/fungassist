<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route ("/backend")
 */
class BackendController extends AbstractController
{
    /**
     * * @Route ("/", name="backend")
     */
    public function index(){

        return $this->render('backend/index.html.twig');
    }

}