<?php

namespace App\Controller;


use App\Repository\QuestionaireRepository;
use App\Repository\QuestionaireResultRepository;
use App\Service\QuestionItemService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(QuestionaireRepository $questionaireRepository, QuestionItemService $questionItemService, QuestionaireResultRepository $questionaireResultRepository)
    {

        $questionaire = $questionaireRepository->findOneBy(['id' => 12]);
        $questionItemParent = $questionItemService->generateQuestionItemTree($questionaire);
        $questionaires = $questionaireRepository->findAll();

        return $this->render('app/index.html.twig', [
            'questionaire' => $questionaire,
            'questionaires' => $questionaires,
            'questionItemParent' => $questionItemParent,
        ]);
    }

    /**
     * @Route("/contact", name="contact_page")
     */
    public function contactPage()
    {
        return $this->render('app/contact.html.twig', [
        ]);
    }

    /**
     * @Route("/about", name="about_page")
     */
    public function aboutPage()
    {
        return $this->render('app/about.html.twig', [
        ]);
    }


    /**
     * Thank you page after successful submission of questionaire
     * @Route("/thankyou", name="thankyou_for_submit")
     */
    public function thankYouSubmitPage()
    {
        return $this->render('app/thankyou.html.twig', [
        ]);
    }

    /**
     * @param QuestionaireRepository $questionaireRepository
     * @param QuestionItemService $questionItemService
     * @return Response
     * @Route("/questionaire/{questionaireTitle}", name="render_questionaire")
     */
    public function renderQuestionaire(string $questionaireTitle, QuestionaireRepository $questionaireRepository, QuestionItemService $questionItemService)
    {

        $questionaires = $questionaireRepository->findAll();

        $questionaire = $questionaireRepository->findOneBy(['title' => $questionaireTitle]);

        $questionItemParent = $questionItemService->generateQuestionItemTree($questionaire);

        return $this->render('app/index.html.twig', [
            'questionaire' => $questionaire,
            'questionaires' => $questionaires,
            'questionItemParent' => $questionItemParent,
        ]);
    }
}
