<?php

namespace App\Controller;


use App\Entity\Anchor;
use App\Repository\AlgorithmRepository;
use App\Repository\AnchorRepository;
use App\Repository\GuidelineRepository;
use App\Repository\QuestionaireRepository;
use App\Repository\QuestionaireResultRepository;
use App\Service\AlgorithmService;
use App\Service\GuidelineService;
use App\Service\QuestionaireResultService;
use App\Service\QuestionItemService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiController
 * @package App\Controller
 * /**
 * @Route("/api/v1")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/restricted/guidelineText", name="api-getGuidelineText")
     */
    public function getGuidelineText(GuidelineRepository $guidelineRepository, GuidelineService $guidelineService)
    {
        $this->denyAccessUnlessGranted('ROLE_CONTRIBUTOR');


        if (isset($_POST['guidelineId'])) {

            $guideline = $guidelineRepository->findOneBy(['id' => $_POST['guidelineId']]);

            return $this->json(['text' => $guidelineService->formatAnchors($guideline)->getText()]);

        } else {
            return $this->json([]);
        }


    }

    /**
     * structure is:
     * questionItemId: current questionItem
     * questionItemIds: all selected questionItems
     * questionaire: questionaireId
     * @Route("/getAnchorByQuestionItem", name="api-getAnchorByQuestionItem")
     */
    public function receiveQuestionItemSelection(AnchorRepository $anchorRepository, SerializerInterface $serializer, AlgorithmService $algorithmService, QuestionaireRepository $questionaireRepository)
    {

        if (isset($_POST['questionItemId']) && isset($_POST['questionItemIds']) && isset($_POST['questionaire'])) {

            // assess whether the user asked for guideline filters
            if (!empty($_POST['guidelineFilter'])) {
                $anchorsQuestionItem = $anchorRepository->findBy(['questionItem' => $_POST['questionItemIds'], 'reviewStatus' => 'OK', 'guideline' => $_POST['guidelineFilter']]);
            } else {
                $anchorsQuestionItem = $anchorRepository->findBy(['questionItem' => $_POST['questionItemIds'], 'reviewStatus' => Anchor::ANCHOR_REVIEW_STATUS['OK']]);
            }

            $questionaire = $questionaireRepository->findOneBy(['id' => $_POST['questionaire']]);

            $positiveAlgorithms = $algorithmService->checkForAlgorithms(array_map('intval', $_POST['questionItemIds']), $questionaire);
            $anchorsAlgorithms = $anchorRepository->findBy(['algorithm' => $positiveAlgorithms, 'reviewStatus' => Anchor::ANCHOR_REVIEW_STATUS['OK']]);

            foreach ($anchorsAlgorithms as $anchor) {
                $anchor->setAlgorithm($algorithmService->checkForRecommendetPathAlgorithms($_POST['questionItemIds'], $anchor->getAlgorithm()));
            }

            $anchors = array_merge($anchorsQuestionItem, $anchorsAlgorithms);

            $jsonObject = $serializer->serialize($anchors, 'json', ['attributes' => ['id', 'text', 'context', 'algorithm' => ['id', 'description', 'choseRecommendedPath'], 'questionItem' => ['id'], 'guideline' => ['id', 'title', 'publishedAt']],
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]);

            /**
             * modify text:
             * - double underline 'not'
             * - ...
             */
            $jsonObject = preg_replace('/ not /', ' <span class=\"dblUnderlined\">not</span> ', $jsonObject);

            return $this->json($jsonObject);

        } else {
            return $this->json([]);
        }

    }

    /**
     * update anchor items via ajax call
     * @Route("/restricted/rejectAnchor", name="api-rejectAnchor", methods={"POST"})
     */
    public function rejectAnchor(AnchorRepository $anchorRepository)
    {

        $this->denyAccessUnlessGranted('ROLE_REVIEWER');

        if (isset($_POST['rejectText']) && isset($_POST['anchorId'])) {

            $anchor = $anchorRepository->findOneBy(['id' => $_POST['anchorId']]);

            $anchor->setReviewCmt($_POST['rejectText']);
            $anchor->setReviewBy($this->getUser());
            $anchor->setReviewAt(new DateTime('now'));
            $anchor->setReviewStatus(Anchor::ANCHOR_REVIEW_STATUS['REJECTED']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anchor);
            $entityManager->flush();

            return $this->json([]);
//            return new \Symfony\Component\HttpFoundation\Response($jsonObject, 200, ['Content-Type' => 'application/json']);

        } else {
            return $this->json([]);
        }

    }

    /**
     * apiEndpoint for sending feedback mails
     * @param MailerInterface $mailer
     * @throws TransportExceptionInterface
     * @Route("/sendFeedback", name="api-sendFeedback", methods={"POST"})
     */
    public function sendFeedback(MailerInterface $mailer)
    {
        // set default replyTo Address and update it if email was provided
        $fromAddress= 'feedback@fungassist.net';
        if($_POST['feedbackReplyTo'] !== ''){
            $fromAddress = $_POST['feedbackReplyTo'];
        }
        $email = (new Email())
            ->from($fromAddress)
            ->to('incoming+infektiologie-ukkoeln-fungassist-13342077-issue-@incoming.gitlab.com')
            ->subject('Feedback Fungassist')
            ->html('<p>' . $_POST['feedbackText'] . '</p>');

        $mailer->send($email);
        return $this->json($_POST, 200);
    }

    /**
     * allow users to get a readable Algorithm statement to better understand why they are seeing certain things.
     * @Route("/getReadableAlgorithmText", name="api-getReadableAlgorithmText")
     */
    public function getReadableAlgorithmText(AlgorithmService $algorithmService, AlgorithmRepository $algorithmRepository)
    {

        $jsonResponse = '';
        if (isset($_POST['algorithmId'])) {
            $algorithm = $algorithmRepository->findOneBy(['id' => $_POST['algorithmId']]);


            $jsonResponse = $algorithmService->generateReadableIfStatement($algorithm);
        }

        return $this->json(json_encode($jsonResponse));
    }

    /**
     * allow users to get a readable Algorithm statement to better understand why they are seeing certain things.
     * @Route("/getOpenCaseCount", name="api-getOpenCaseCount")
     */
    public function getOpenCaseCount(QuestionaireResultRepository $questionaireResultRepository, QuestionaireRepository $questionaireRepository){

        $questionaire = $questionaireRepository->findOneBy(['title'=>$_POST['questionaire']]);

        $case['url']= $this->generateUrl('case_index_questionaire', array('questionaire' => $questionaire));
        $case['caseCount']= count($questionaireResultRepository->resultsPastMonth($questionaire));

        return $this->json(json_encode($case));
    }

    /**
     * receive a version of the form on each click
     * @Route("/submitQuestionaire", name="api-submitQuestionaire")
     */
    public function submitQuestionaire(QuestionaireResultService $questionaireResultService){
        $questionaireResult = $questionaireResultService->updateQuestionaireResult($_POST);
        return $this->json('');
    }

    /**
     * reserve a specific slug for submitting questionaires
     * @Route("/getNewQuestionaireResultSlug", name="api-getNewQuestionaireResultSlug")
     */
    public function getNewQuestionaireResultSlug(QuestionaireResultService $questionaireResultService){
        $questionaireResult = $questionaireResultService->newQuestionaireResult($_POST['questionaireTitle']);

        return $this->json($questionaireResult->getSlug());
    }

    /**
     * reserve a specific slug for submitting questionaires
     * @Route("/getPatientReport", name="api-getPatientReport")
     */
    public function getPatientReport(QuestionaireResultRepository $questionaireResultRepository, QuestionItemService $questionItemService){
        $patientReport = '';
        if(isset($_POST['slug'])){
            $questionaireResult = $questionaireResultRepository->findOneBy(['slug'=> $_POST['slug'], 'cmt'=> null]);

            $patientReport = $questionItemService->generatePatientReport($questionaireResult);
        }
        return $this->json($patientReport);

    }

    /**
     * reserve a specific slug for submitting questionaires
     * @Route("/getActiveQuestionItems", name="api-getActiveQuestionItems")
     */
    public function getActiveQuestionItems(QuestionaireRepository $questionaireRepository, QuestionItemService $questionItemService){
        $result = $questionItemService->assessQuestionItemsToDisplay($_POST['selectedQuestionItemIds'], $questionaireRepository->findOneBy(['title'=>$_POST['questionaireTitle']]));

        return $this->json($result);
    }
}
