<?php

namespace App\Controller;

use App\Entity\QuestionItem;
use App\Form\QuestionItemType;
use App\Repository\QuestionaireRepository;
use App\Repository\QuestionItemRepository;
use App\Service\QuestionItemService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/questionitem")
 */
class QuestionItemController extends AbstractController
{
    /**
     * @Route("/", name="question_item_index", methods={"GET"})
     */
    public function index(QuestionaireRepository $questionaireRepository): Response
    {
        return $this->render('question_item/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll(),
        ]);
    }

    /**
     *  Find all question items for a given questionaireId
     * @Route("/questionaire/{questionaireId}", name="question_item_index_by_questionaire", methods={"GET"})
     */
    public function indexByQuestionaire(int $questionaireId, QuestionaireRepository $questionaireRepository, QuestionItemService $questionItemService): Response
    {
        $questionaire = $questionaireRepository->findOneBy(['id' => $questionaireId]);

        $questionItemParent = $questionItemService->generateQuestionItemTree($questionaire);

        return $this->render('question_item/index_by_questionaire.html.twig', [
            'questionaire' => $questionaire,
            'questionItemParent' => $questionItemParent,
        ]);
    }

    /**
     * @Route("/new/parent/{parentId}/{type}", name="question_item_new", methods={"GET","POST"})
     */
    public function new(int $parentId, string $type, Request $request, QuestionItemRepository $questionItemRepository, QuestionItemService $questionItemService): Response
    {
        $parentQuestionItem = $questionItemRepository->findOneBy(['id' => $parentId]);

        // stop wrong questionItems of getting childs
        if ($parentQuestionItem->getType() == QuestionItem::TYPE['OPTION']) {
            throw new HttpException('This QuestionItem cannot have childs');
        }

        //prefill the questionitem
        $questionItem = new QuestionItem();
        $questionItem->setParentId($parentId);
        $questionItem->setQuestionaire($parentQuestionItem->getQuestionaire());

        switch ($type) {
            case QuestionItem::TYPE['MULTI_SELECT']:
                $questionItem->setType(QuestionItem::TYPE['MULTI_SELECT']);
                break;
            case QuestionItem::TYPE['SINGLE_SELECT']:

                $questionItem->setType(QuestionItem::TYPE['SINGLE_SELECT']);
                break;
            case QuestionItem::TYPE['OPTION']:
                $questionItem->setType(QuestionItem::TYPE['OPTION']);
                break;
            default:
                throw new HttpException('QuestionItemType unkown');
        }

        $questionItem = $questionItemService->setItemAboveId($questionItem, $parentQuestionItem);

        $form = $this->createForm(QuestionItemType::class, $questionItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($questionItem);
            $entityManager->flush();

            return $this->redirectToRoute('question_item_index_by_questionaire', ['questionaireId' => $questionItem->getQuestionaire()->getId()]);
        }

        return $this->render('question_item/new.html.twig', [
            'type' => $type,
            'parentQuestionItem' => $parentQuestionItem,
            'questionItem' => $questionItem,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="question_item_show", methods={"GET"})
     */
    public function show(QuestionItem $questionItem): Response
    {
        return $this->render('question_item/show.html.twig', [
            'questionItem' => $questionItem,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="question_item_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, QuestionItem $questionItem, QuestionItemRepository $questionItemRepository): Response
    {
        if ($questionItem->getType() === QuestionItem::TYPE['PARENT']) {
            $questionItem->setParentId(0);
            $form = $this->createForm(QuestionItemType::class, $questionItem);
        } else {
            $possibleParents = $questionItemRepository->findBy(['questionaire' => $questionItem->getQuestionaire()]);
            foreach ($possibleParents as $parent) {
                $parrentArray[$parent->getId()] = $parent->__toString();
            }
            $form = $this->createForm(QuestionItemType::class, $questionItem, ['empty_data' => ['possibleParents' => $parrentArray]]);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('question_item_index');
        }

        return $this->render('question_item/edit.html.twig', [
            'questionItem' => $questionItem,
            'questionItemParents' => $questionItemRepository->getPossibleQuestionItemParents($questionItem->getQuestionaire()->getId()),
            'questionItems' => $questionItemRepository->findBy(['questionaire' => $questionItem->getQuestionaire()]),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="question_item_delete", methods={"DELETE"})
     */
    public function delete(Request $request, QuestionItem $questionItem): Response
    {
        if ($this->isCsrfTokenValid('delete' . $questionItem->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($questionItem);
            $entityManager->flush();
        }

        return $this->redirectToRoute('question_item_index');
    }

    /**
     * @Route("/moveup/{questionaireId}/{id}", name="question_item_moveup", methods={"GET","POST"})
     */
    public function moveUp(QuestionItem $questionItem, QuestionItemService $questionItemService, int $questionaireId)
    {

        if ($questionItemService->moveQuestionItemUp($questionItem)) {

            return $this->redirectToRoute('question_item_index_by_questionaire', [
                'questionaireId' => $questionaireId,
            ]);
        }
    }

    /**
     * @Route("/movedown/{questionaireId}/{id}", name="question_item_movedown", methods={"GET","POST"})
     */
    public function moveDown(QuestionItem $questionItem, QuestionItemService $questionItemService, int $questionaireId)
    {
        if ($questionItemService->moveQuestionItemDown($questionItem)) {


            return $this->redirectToRoute('question_item_index_by_questionaire', [
                'questionaireId' => $questionaireId,
            ]);
        }
    }
}
