<?php

namespace App\Controller;

use App\Entity\GuidelineSource;
use App\Form\GuidelineSourceType;
use App\Repository\GuidelineSourceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/guidelinesource")
 */
class GuidelineSourceController extends AbstractController
{
    /**
     * @Route("/", name="guideline_source_index", methods={"GET"})
     */
    public function index(GuidelineSourceRepository $guidelineSourceRepository): Response
    {
        return $this->render('guideline_source/index.html.twig', [
            'guideline_sources' => $guidelineSourceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="guideline_source_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $guidelineSource = new GuidelineSource();
        $form = $this->createForm(GuidelineSourceType::class, $guidelineSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guidelineSource);
            $entityManager->flush();

            return $this->redirectToRoute('guideline_source_index');
        }

        return $this->render('guideline_source/new.html.twig', [
            'guideline_source' => $guidelineSource,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_source_show", methods={"GET"})
     */
    public function show(GuidelineSource $guidelineSource): Response
    {
        return $this->render('guideline_source/show.html.twig', [
            'guideline_source' => $guidelineSource,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guideline_source_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GuidelineSource $guidelineSource): Response
    {
        $form = $this->createForm(GuidelineSourceType::class, $guidelineSource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guideline_source_index');
        }

        return $this->render('guideline_source/edit.html.twig', [
            'guideline_source' => $guidelineSource,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_source_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GuidelineSource $guidelineSource): Response
    {
        if ($this->isCsrfTokenValid('delete' . $guidelineSource->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guidelineSource);
            $entityManager->flush();
        }

        return $this->redirectToRoute('guideline_source_index');
    }
}
