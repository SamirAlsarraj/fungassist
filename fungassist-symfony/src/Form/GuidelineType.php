<?php

namespace App\Form;

use App\Entity\Guideline;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class GuidelineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        dump($builder);die;
        $builder
            ->add('title')
            ->add('authors')
            ->add('text', CKEditorType::class)
            ->add('organization')
            ->add('abstract')
            ->add('appendix')
            ->add('acknowledgements')
            ->add('publishedAt', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy',
                'label' => 'Year published'
            ])
            ->add('publishedBy')
            ->add('publishedIn')
            ->add('doi')
            ->add('conflictsOfInterest')
            ->add('sponsoring')
            ->add('cmt')
            ->add('recommendationLevels')
            ->add('guidelineSources')
            ->add('evidenceLevels');
//            ->add('recommendationLevels',EntityType::class, ['class'=> RecommendationLevel::class, 'multiple'=>true, 'attr' => array('class' => 'chzn-select')])
//            ->add('guidelineSources',EntityType::class, ['class'=> GuidelineSource::class, 'multiple'=>true, 'attr' => array('class' => 'chzn-select')])
//            ->add('evidenceLevels',EntityType::class, ['class'=> EvidenceLevel::class, 'multiple'=>true, 'attr' => array('class' => 'chzn-select')]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Guideline::class,
        ]);
    }
}
