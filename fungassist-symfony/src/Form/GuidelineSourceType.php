<?php

namespace App\Form;

use App\Entity\GuidelineSource;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GuidelineSourceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quoteNumber')
            ->add('articleQuoteString')
            ->add('pubmedId')
            ->add('guidelines');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GuidelineSource::class,
        ]);
    }
}
