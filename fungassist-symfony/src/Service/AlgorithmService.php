<?php


namespace App\Service;


use App\Entity\Algorithm;
use App\Entity\Questionaire;
use App\Entity\QuestionItem;
use App\Repository\AlgorithmRepository;
use App\Repository\QuestionItemRepository;
use JWadhams\JsonLogic;

class AlgorithmService
{
    /**
     * define the maximum amount of levels an algorithms goes when inheriting other algorithms
     */
    const MAX_ALGORITHM_INHERITANCE = 5;

    private $algorithmRepository;

    private $questionItemRepository;

    /**
     * this var counts how many algorithms are processed in one session
     * @var int
     */
    private $parseAlgorithmCount = 0;

    public function __construct(AlgorithmRepository $algorithmRepository, QuestionItemRepository $questionItemRepository)
    {
        $this->algorithmRepository = $algorithmRepository;
        $this->questionItemRepository = $questionItemRepository;
    }


    /**
     * convert the database if-statement to an algorithm and return true / false
     * @param Algorithm $algorithm
     * @return bool
     */
    public function executeAlgorithm(string $ifStatement, array $selectedQuestionItems)
    {
        $parsedIfStatement = $this->parseAlgorithm($ifStatement);
        // Use JSONLogic to execute the algorithm If Statement and compare it to the selected QuestionItems
        $selectedQuestionItems = array_fill_keys($selectedQuestionItems, true);

        // reset Count for parsed algorithms per execution
        $this->parseAlgorithmCount = 0;

        return JsonLogic::apply(json_decode($parsedIfStatement, true), $selectedQuestionItems);

    }

    /**
     * @param $algorithmString
     * @return string
     * @throws \ErrorException
     */
    public function parseAlgorithm($algorithmString){

        if($this->parseAlgorithmCount > AlgorithmService::MAX_ALGORITHM_INHERITANCE){
            throw new \ErrorException('Your algorithm includes too many references');
        }


        //       {\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}
        preg_match_all('/{\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}/', $algorithmString, $matches);
        foreach ($matches[1] as $match){
            // find the relevant algorithm
            $algorithm = $this->algorithmRepository->findOneBy(['id'=>$match]);
            $algorithmString = preg_replace('/{\s*"\s*algorithm\s*"\s*:\s*'.$match.'\s*}/', $algorithm->getIfStatement(), $algorithmString);
        }

        // Increase the parse Algorithm Count to ensure no loops
        $this->parseAlgorithmCount++;

        // check for new referenced algorithms
        preg_match_all('/{\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}/', $algorithmString, $matches);
        if(!empty($matches[0])){
            $this->parseAlgorithm($algorithmString);
        } else {
            return $algorithmString;
        }
    }

    /**
     * @param Algorithm $algorithm
     * @return mixed
     */
    private function getQuestionItemStatesForAlgorithm(Algorithm $algorithm)
    {
        // convert database string to array
        $algorithmArray = json_decode($algorithm->getIfStatement(), true);

        // got to the if statement part
        $algorithmArray = $algorithmArray['if'];

        // convert questionItemIds from string to numbers
        foreach ($algorithmArray as $value) {

            // check whether value is an operator or not
            if ($value != '&&' && $value != '||') {

                $value = intval($value);
                if ($value > 0) {
                    $algorithmQuestionItems[$value] = true;
                } else {
                    $value = $value * -1;
                    $algorithmQuestionItems[$value] = false;
                }
            }
        }

        return $algorithmQuestionItems;
    }

    /**
     * determine the depth of an arbitrary array
     * https://stackoverflow.com/questions/262891/is-there-a-way-to-find-out-how-deep-a-php-array-is
     *
     * @param $array
     * @return int
     */
    public function arrayDepth($array)
    {

        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->arrayDepth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;

    }


    /**
     * remove unnecessary parts of the array such as not clicked items
     * @param $inputArray
     * @return array
     */
    private function cleanInputArray($inputArray)
    {


        // remove all null array items
        $result = array_filter($inputArray, function ($value) {

            return !is_null($value);

        });

        // remove all items that are not necessary (e.g. text, headlines etc.)

        return $result;

    }

    /**
     * Return those algorithms that can be run with the current user input
     * @param $questionItemArray
     * @param $algorithms
     * @return array
     */
    private function checkInputSufficiencyForAlgorithm($questionItemArray, $algorithms)
    {

        // TODO: JSON might include OR statements such that not all items will be needed...

        $algorithmsToRun = array();

        foreach ($algorithms as $algorithm) {
            $questionItemStates = $this->getQuestionItemStatesForAlgorithm($algorithm);

            if (empty(array_diff_key($questionItemStates, $questionItemArray))) {
                $algorithmsToRun[$algorithm] = true;
            }

        }
        return $algorithmsToRun;
    }

    /**
     * Input array structure ["questionaire"=>"questionaireID", "items"=>["itemID"=>"true/false/null", "itemId2"=> "true / false / null", ...]]
     *
     * @param $questionItemArray
     * @param Questionaire $questionaire
     * @return [Algorithm]
     */
    public function checkForAlgorithms(array $questionItemArray, $questionaire)
    {
        // array with anchorIds
        $positiveAlgorithms = array();

        // get all algorithms associated with this questionaire
        $algorithms = $this->algorithmRepository->findBy(['questionaire' => $questionaire]);

        // check whether the input array provides sufficient information for running the algorithm
//        $algorithmsToRun = $this->checkInputSufficiencyForAlgorithm($questionItemArray, $algorithms);

        // execute each algorithm
        foreach ($algorithms as $algorithm) {
            if ($this->executeAlgorithm($algorithm->getIfStatement(), $questionItemArray)) {
                $positiveAlgorithms [] = $algorithm;
            }
        }
        return $positiveAlgorithms;
    }

    /**
     * @param array $questionItemArray
     * @param Algorithm $positiveAlgorithm
     * @return mixed
     */
    public function checkForRecommendetPathAlgorithms(array $questionItemArray, $positiveAlgorithm){

        if($positiveAlgorithm->getRecommendedPath() !== "" && is_string($positiveAlgorithm->getRecommendedPath())){
            if ($this->executeAlgorithm($positiveAlgorithm->getRecommendedPath(), $questionItemArray)) {
                $positiveAlgorithm->setChoseRecommendedPath('true');
            } else{
                $positiveAlgorithm->setChoseRecommendedPath('false');
            }
        } else{
            $positiveAlgorithm->setChoseRecommendedPath("");
        }
        return $positiveAlgorithm;
    }


    /**
     * function to generate a readable text statement from the JSON Logic syntax for a given Algorithm object
     * @param Algorithm $algorithm
     * @return Algorithm
     */
    public function generateReadableIfStatement(Algorithm $algorithm)
    {

        // prepare if statement for inherited algorithms
        $algorithmIfStatement = $this->parseAlgorithm($algorithm->getIfStatement());

        // reset counter
        $this->parseAlgorithmCount=0;


        $json = json_decode($algorithmIfStatement, true);


        $implodedIfStatement = $this->multi_implode($json, ' ');

        preg_match_all('!\d+!', $implodedIfStatement, $matches);

        $questionItems = $this->questionItemRepository->findBy(['id' => array_map('intval', $matches[0])]);

        foreach ($questionItems as $questionItem) {
            $parentQuestionitem = $this->questionItemRepository->findOneBy(['id' => $questionItem->getParentId()]);

            $cmt = $this->addCmtToReadableQuestionItem($questionItem, $algorithm);
            $implodedIfStatement = str_replace('--' . $questionItem->getId() . '--', strtolower($parentQuestionitem->getContent() . ': <strong>' . $questionItem->getContent() . ' '.$cmt.'</strong>'), $implodedIfStatement);
        }
        return $implodedIfStatement;
    }

    /**
     * add cmts to the readable form of a questionItem
     * @param QuestionItem $questionItem
     * @param Algorithm $algorithm
     * @return string
     */
    private function addCmtToReadableQuestionItem(QuestionItem $questionItem, Algorithm $algorithm){
        $cmt = [];

        if($this->checkForFalseQuestionItem( $questionItem,  $algorithm)){
            $cmt []= "(IF NOT TRUE!)";
        }

        if($this->questionItemInRecommendedPath( $questionItem,  $algorithm)){
            $cmt []= "(Recommended Path)";
        }


        return implode(' ', $cmt);
    }

    /**
     * @param QuestionItem $questionItem
     * @param Algorithm $algorithm
     * @return bool
     */
    private function questionItemInRecommendedPath(QuestionItem $questionItem, Algorithm $algorithm){
        return strpos($algorithm->getRecommendedPath(), '"'.$questionItem->getId().'"');
    }

    /**
     * @param QuestionItem $questionItem
     * @param Algorithm $algorithm
     * @return bool
     */
    private function checkForFalseQuestionItem(QuestionItem $questionItem, Algorithm $algorithm){
        preg_match('/"'.$questionItem->getId().'"\s*}\s*,\s*f/', $algorithm->getIfStatement(), $m);

        if(empty($m)){
            return false;
        } else{
            return true;
        }
    }

    /**
     * provide an array of readable if statements with the algorithm's id as index
     * @param [Algorithm] $algorithms
     * @return array
     */
    public function getReadableIfStatementsArray($algorithms)
    {
        $readableIfStatements = [];

        foreach ($algorithms as $algorithm) {
            $readableIfStatements[$algorithm->getId()] = $this->generateReadableIfStatement($algorithm);
        }

        return $readableIfStatements;
    }

    /**
     * @param $array array
     * @param $glue string
     * @return bool|string
     */
    private function multi_implode($array, $glue)
    {
        $ret = '';

        foreach ($array as $key => $item) {
            if (is_array($item)) {

                $ret .= ' ' . $this->multi_implode($item, '<br><br>' . strtoupper($key) . '<br><br>') . $glue;

            } else {
                if ($key === 'var') {
                    $ret .= '--' . $item . '-- ' . $glue;
                }

            }
        }

        $ret = substr($ret, 0, 0 - strlen($glue));

        return $ret;
    }
}