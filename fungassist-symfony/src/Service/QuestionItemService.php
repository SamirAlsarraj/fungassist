<?php


namespace App\Service;


use App\Entity\Questionaire;
use App\Entity\QuestionaireResult;
use App\Entity\QuestionItem;
use App\Repository\QuestionItemRepository;
use Doctrine\ORM\EntityManagerInterface;

class QuestionItemService
{
    private $questionItemRepository;

    private $entityManager;

    private $algorithmService;

    public function __construct(QuestionItemRepository $questionItemRepository, EntityManagerInterface $entityManager, AlgorithmService $algorithmService)
    {
        $this->questionItemRepository = $questionItemRepository;
        $this->entityManager = $entityManager;
        $this->algorithmService = $algorithmService;
    }

    /**
     * @param QuestionItem $questionItem
     * @return QuestionItem
     */
    public function getChildrenItems(QuestionItem $questionItem)
    {
        if(empty($questionItem->getChildren())){

            // find a item where parentID = $questionItem->getId() AND WHERE itemAbove = $questionItem->getId()
            $previousQuestionItemId = $questionItem->getId();
            $i = 1;
            while ($i != null) {

                // find the next item
                $nextQuestionItem = $this->questionItemRepository->findOneBy(['parentId' => $questionItem->getId(), 'itemAbove' => $previousQuestionItemId]);
                // add it to the list of children
                if ($nextQuestionItem != null) {
                    $questionItem->addChild($nextQuestionItem);
                    // set the newly found QuestionItem for the next query
                    $previousQuestionItemId = $nextQuestionItem->getId();
                } else {
                    $i = null;
                }


            }

            return $questionItem;
        } else{
            return $questionItem;
        }


    }

    /**
     * generate Questionitem Tree within the parent questionItem as children
     * @param Questionaire $questionaire
     * @return QuestionItem|null
     */
    public function generateQuestionItemTree(Questionaire $questionaire)
    {

        $questionItemParent = $this->questionItemRepository->findOneBy(['questionaire' => $questionaire, 'type' => QuestionItem::TYPE['PARENT']]);

        // get all selectables

        $questionItemParent = $this->getChildrenItems($questionItemParent);
        if ($questionItemParent->getChildren() != null) {
            // get all options
            foreach ($questionItemParent->getChildren() as $questionItem) {
                if ($questionItem != null) {
                    $questionItem = $this->getChildrenItems($questionItem);
                }
            }
        }

        return $questionItemParent;
    }

    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function moveQuestionItemUp(QuestionItem $questionItem)
    {

        // QuestionItem is already on top, then do nothing
        if ($questionItem->getParentId() == $questionItem->getItemAbove()) {
            return true;
        }

        // get relevant QuestionItems
        $questionItemAbove = $this->questionItemRepository->findOneBy(['id' => $questionItem->getItemAbove()]);
        $questionItemUnderneath = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItem->getId()]);


        if ($questionItemUnderneath == null) {
            $questionItem->setItemAbove($questionItemAbove->getItemAbove());
            $questionItemAbove->setItemAbove($questionItem->getId());

            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemAbove);
        } else {
            $questionItem->setItemAbove($questionItemAbove->getItemAbove());
            $questionItemAbove->setItemAbove($questionItem->getId());
            $questionItemUnderneath->setItemAbove($questionItemAbove->getId());
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemAbove);
            $this->entityManager->persist($questionItemUnderneath);
        }

        $this->entityManager->flush();

        return true;

    }


    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function moveQuestionItemDown(QuestionItem $questionItem)
    {
        // get relevant QuestionItems
        $questionItemUnderneath = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItem->getId()]);
        $questionItemUnderneathTwo = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItemUnderneath->getId()]);

        // question item is the last item
        if ($questionItemUnderneath == null) {
            return true;
        }

        // just one item below the item
        if ($questionItemUnderneathTwo == null) {
            $questionItemUnderneath->setItemAbove($questionItem->getItemAbove());
            $questionItem->setItemAbove($questionItemUnderneath->getId());
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemUnderneath);
        } else {

            // two or more items below the item
            $questionItemUnderneath->setItemAbove($questionItem->getItemAbove());
            $questionItem->setItemAbove($questionItemUnderneath->getId());
            $questionItemUnderneathTwo->setItemAbove($questionItem->getId());
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemUnderneath);
            $this->entityManager->persist($questionItemUnderneathTwo);
        }

        $this->entityManager->flush();

        return true;

    }

    /**
     * find the correct itemAbove Id for the given $questionItem and return the object with it's correct itemAbove Property set.
     * @param QuestionItem $questionItem
     * @return QuestionItem
     */
    public function setItemAboveId(QuestionItem $questionItem, QuestionItem $parent)
    {

        $parent = $this->getChildrenItems($parent);

        // set itemAbove to parent if parent has no children, else use the last child
        if (empty($parent->getChildren())) {
            $questionItem->setItemAbove($parent->getId());
        } else {
            /**
             * @var $lastChild QuestionItem
             */
            $children = $parent->getChildren();
            $lastChild = end($children);
            $questionItem->setItemAbove($lastChild->getId());
        }

        return $questionItem;
    }

    /**
     * generate a text case Report
     * @param QuestionaireResult $questionaireResult
     * @return bool|mixed|string|null
     */
    public function generatePatientReport(QuestionaireResult $questionaireResult){

        if($questionaireResult->getQuestionaire()->getCaseReportTemmplate() === null){
            return false;
        } else{
            $patientReport = $questionaireResult->getQuestionaire()->getCaseReportTemmplate();

            $selectedQuestionItems = [];
            foreach($questionaireResult->getSelectedQuestionItems() as $questionItem){
                $selectedQuestionItems[$questionItem->getId()]=$questionItem;
            }


            // search for all %%questionId%% to replace stuff
            preg_match_all('/(?<=%%)\d+(?=%%)/', $patientReport, $matches);


            // loop over the found spots
            foreach($matches[0] as $match){
                $hits=[];

                // get all childs of the parent questionItem
                $parent = $this->getChildrenItems($this->questionItemRepository->findOneBy(['id'=>$match]));

                // check which of these is selected in the questionaireResult object
                foreach($parent->getChildren() as $child){
                    if(array_key_exists($child->getId(), $selectedQuestionItems)){
                        $hits[]=$child->getContent();
                    }
                }

                // implode the content of these
                $string = implode(', ', $hits);

                // replace the %%questionId%% with this string
                $patientReport = str_replace('%%'.$match.'%%', $string, $patientReport);


            }
            return $patientReport;
        }

    }

    /**
     * @param array $selectedQuestionItems
     * @param Questionaire $questionaire
     * @return array
     */
    public function assessQuestionItemsToDisplay($selectedQuestionItemIds, Questionaire $questionaire){
        // get all questionItems of the questionaire
        $questionItems = $this->questionItemRepository->findBy(['questionaire'=>$questionaire]);

        // loop over them and execute the displayCondition with the given $selectedQuestionItems
        $assessedQuestionItems = [];
        foreach($questionItems as $questionItem){
            if($questionItem->getDisplayCondition() == null){
                // if no condition was set display this questionItem
                $assessedQuestionItems[]=['id'=>$questionItem->getId(), 'state'=>true];
            } else {
                // otherwise execute the algorithm
                $assessedQuestionItems[]=$assessedQuestionItems[]=['id'=>$questionItem->getId(), 'state'=>$this->algorithmService->executeAlgorithm($questionItem->getDisplayCondition(), $selectedQuestionItemIds)];
            }
        }

        // return an array with 'questionItemId'=>bool
        return $assessedQuestionItems;

        }
}