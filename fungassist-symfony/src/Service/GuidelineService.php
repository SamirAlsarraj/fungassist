<?php


namespace App\Service;

use App\Entity\Anchor;
use App\Entity\Guideline;
use App\Repository\GuidelineRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class GuidelineService
 * @package App\Service
 */
class GuidelineService
{

    private $guidelineRepository;

    private $anchorService;

    private $em;

    public function __construct(GuidelineRepository $guidelineRepository, AnchorService $anchorService, EntityManagerInterface $em)
    {
        $this->guidelineRepository = $guidelineRepository;
        $this->anchorService = $anchorService;
        $this->em = $em;
    }

    /**
     * format Anchors of a guideline with highlightning and links
     * @param Guideline $guideline
     * @return Guideline
     */
    public function formatAnchors(Guideline $guideline)
    {
        // preg_match('/=====start_anchor_(\d+)_start=====(.+)=====end_anchor_\1_end=====/s', $guidelineText,$matches);

        // get all anchors associated with this guideline and replace the anchors with formatted text

        $anchors = $guideline->getAnchors();
        $formattedGuidelineText = $guideline->getText();

        foreach ($anchors as $anchor) {
            ;
            $formattedGuidelineText = preg_replace([
                '/' . $this->anchorService->createAnchorStartString($anchor->getId()) . '/',
                '/' . $this->anchorService->createAnchorEndString($anchor->getId()) . '/',
            ], [
                '<span title="Anchor to Question-Item: ' . $anchor->getQuestionItem() . '" class="formatted-anchor">',
                '</span><a target="_blank" href="backend/anchor/' . $anchor->getId() . '">&#8618;</a>',
            ], $formattedGuidelineText);
        }

        return $guideline->setFormattedText($formattedGuidelineText);
    }

    /**
     * Add an anchor to its guideline by using the special anchor Formatting
     * @param Anchor $anchor
     */
    public function addAnchorToGuideline(Anchor $anchor)
    {
        $guideline = $anchor->getGuideline();
        $guideline->setText(str_replace($anchor->getText(), $this->anchorService->createAnchorString($anchor->getText(), $anchor->getId()), $guideline->getText()));
        $this->em->persist($guideline);
        $this->em->flush();
    }

    /**
     * remove a given anchor from its guideline
     * @param Anchor $anchor
     */
    public function removeAnchorFromGuideline(Anchor $anchor)
    {
        $guideline = $anchor->getGuideline();

        // replace anchor with empty string
        $guidelinetextWithoutAnchor = $this->anchorService->removeAnchorFromString($anchor->getId(), $guideline->getText());

        $guideline->setText($guidelinetextWithoutAnchor);
        $this->em->persist($guideline);
        $this->em->flush();
    }
}