<?php


namespace App\Service;


use App\Entity\Guideline;
use App\Entity\GuidelineHistory;
use App\Repository\GuidelineHistoryRepository;
use App\Repository\GuidelineRepository;
use cogpowered\FineDiff\Diff;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class GuidelineHistoryService
{

    /**
     * @var GuidelineRepository
     */
    private $guidelineRepository;

    /**
     * @var GuidelineHistoryRepository
     */
    private $guidelineHistoryRepository;

    private $entityManager;

    public function __construct(GuidelineRepository $guidelineRepository, GuidelineHistoryRepository $guidelineHistoryRepository, EntityManagerInterface $entityManager)
    {
        $this->guidelineRepository = $guidelineRepository;
        $this->guidelineHistoryRepository = $guidelineHistoryRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * This is the function that always should be used to update guidelines. We make sure that a copy of the old entry
     * is made to the GuidelineHistory Table and then update the GuidelineTable accordingly.
     * @param Guideline $guideline
     */
    public function updateGuideline(Guideline $modifiedGuideline)
    {

        // instatiate a GuidelineHistory Object
        $guidelineHistory = new GuidelineHistory();

        // get the current database-version of the modifiedGuideline
        $currentGuidelineData = $this->entityManager->getUnitOfWork()->getOriginalEntityData($modifiedGuideline);

        // copy the Guideline Objects content to a GuidelineHistory Object
        $guidelineHistory->setTitle($currentGuidelineData['title']);
        $guidelineHistory->setAuthors($currentGuidelineData['authors']);
        $guidelineHistory->setGuideline($modifiedGuideline);
        $guidelineHistory->setRevisionDate(new DateTime('now'));
        $guidelineHistory->setText($currentGuidelineData['text']);

        // find and set correct versionCount
        $latestGuidelineVersion = $this->guidelineHistoryRepository->findMaxVersion($modifiedGuideline);

        if ($latestGuidelineVersion == null) {
            $guidelineHistory->setVersionCount(1);
        } else {
            $guidelineHistory->setVersionCount(intval($latestGuidelineVersion) + 1);
        }

        // persist currentGuideline in GuidelineHistory table
        $this->entityManager->persist($guidelineHistory);

        // update guideline in actual guideline Table
        $this->entityManager->persist($modifiedGuideline);

        $this->entityManager->flush();

    }


    /**
     * returns a nice view of the difference between two versions of a guideline
     * @param $guideline
     */
    public function diffToHistory(Guideline $guideline)
    {

        $diff = new Diff();
        $diffAray = array();
        $guidelineHistories = $guideline->getGuidelineHistories()->getValues();

        foreach ($guidelineHistories as $guidelineHistory) {
            $diffAray[$guidelineHistory->getVersionCount()] = html_entity_decode($diff->render($guidelineHistory->getText(), $guideline->getText()));
        }
        return $diffAray;


    }

}