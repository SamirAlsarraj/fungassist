<?php

namespace App\Command;

use App\Entity\GuidelineSource;
use App\Repository\GuidelineRepository;
use App\Repository\GuidelineSourceRepository;
use App\Service\GuidelineSourceService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Console command to add many sources of one article at once by supplying a textfile with a article sources per line
 *
 * Class GuidelineSourceUploadCommand
 * @package App\Command
 */
class GuidelineSourceUploadCommand extends Command
{
    protected static $defaultName = 'guideline:source:upload';

    /**
     * @var GuidelineSourceRepository
     */
    private $guidelineSourceRepository;

    /**
     * @var GuidelineRepository
     */
    private $guidelineRepository;

    /**
     * @var GuidelineSourceService
     */
    private $guidelineSourceService;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $projectDir;

    public function __construct(EntityManagerInterface $em, GuidelineSourceRepository $guidelineSourceRepository, GuidelineRepository $guidelineRepository, GuidelineSourceService $guidelineSourceService, string $projectDir, string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->guidelineSourceRepository = $guidelineSourceRepository;
        $this->guidelineRepository = $guidelineRepository;
        $this->guidelineSourceService = $guidelineSourceService;
        $this->projectDir = $projectDir;

    }

    protected function configure()
    {
        $this
            ->setDescription('Command for batch-uploading many sources from one guideline')
            ->addArgument('guidelineId', InputArgument::REQUIRED, 'ID of the relevant Guideline')
            ->addArgument('guidelineSourcesFileName', InputArgument::REQUIRED, 'Name of the .txt file with a guidelinesource in each line (needs to be stored in /public/resources/guideline_sources_batch)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('guidelineId');
        $arg2 = $input->getArgument('guidelineSourcesFileName');

        // get the textfile from the public directory and create an array from it
        $textFileDirectory = $this->projectDir . '/public/resources/guideline_sources_batch/';
        $guidelineSources = file($textFileDirectory . $arg2);

        // get relevant guideline from database
        $guideline = $this->guidelineRepository->findOneBy(['id' => $arg1]);

        // for batching database writes
        $batchCounter = 0;

        // loop over all the array items and query pubmed for the article source string
        foreach ($guidelineSources as $key => $source) {
            $entity = new GuidelineSource();
            $entity->addGuideline($guideline);
            $entity->setArticleQuoteString($source);
            $entity->setQuoteNumber($key + 1);

            // search on pubmed for the string provided
            $pmid = $this->guidelineSourceService->queryForPMIDOnPubMed($source);


            // multiple modifications for improving search results on pubmed
            if (empty($pmid)) {
                $newSource = preg_replace('/(?<=,).*(?=,)/', '', $source);
                $io->note($newSource);
                $pmid = $this->guidelineSourceService->queryForPMIDOnPubMed($newSource);
            }
            if (empty($pmid)) {
                $newSource = preg_replace('/(?<=,).*?et al/', '', $source);
                $io->note($newSource);
                $pmid = $this->guidelineSourceService->queryForPMIDOnPubMed($newSource);
            }
            if (empty($pmid)) {
                $newSource = preg_replace('/(?<=\d{4});.*/', '', $source);
                $newSource = preg_replace('/(?<=,).*?et al/', '', $newSource);
                $io->note($newSource);
                $pmid = $this->guidelineSourceService->queryForPMIDOnPubMed($newSource);
            }
            if (empty($pmid)) {
                $newSource = preg_replace('/(?<=\d{4});.*/', '', $source);
                $newSource = preg_replace('/(?<=,).*(?=,)/', '', $newSource);
                $io->note($newSource);
                $pmid = $this->guidelineSourceService->queryForPMIDOnPubMed($newSource);
            }

            if (!empty($pmid)) {
                $entity->setPubmedId(implode($pmid));
            }

            $io->note($entity->getQuoteNumber() . ': ' . $entity->getPubmedId());
            $batchCounter++;
            $this->em->persist($entity);
            if ($batchCounter >= 15) {
                $this->em->flush();
                $batchCounter = 0;
            }
        }
        $this->em->flush();


        $io->success('Successfully inserted into database');
    }
}
