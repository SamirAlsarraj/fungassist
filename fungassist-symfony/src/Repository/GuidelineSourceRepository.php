<?php

namespace App\Repository;

use App\Entity\GuidelineSource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GuidelineSource|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuidelineSource|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuidelineSource[]    findAll()
 * @method GuidelineSource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuidelineSourceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GuidelineSource::class);
    }

    // /**
    //  * @return GuidelineSource[] Returns an array of GuidelineSource objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuidelineSource
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
