<?php

namespace App\Repository;

use App\Entity\AnchorTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AnchorTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method AnchorTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method AnchorTag[]    findAll()
 * @method AnchorTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnchorTagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AnchorTag::class);
    }

    // /**
    //  * @return AnchorTag[] Returns an array of AnchorTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AnchorTag
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
