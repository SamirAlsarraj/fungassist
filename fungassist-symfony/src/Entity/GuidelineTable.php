<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuidelineTableRepository")
 */
class GuidelineTable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Guideline", inversedBy="guidelineTables")
     * @ORM\JoinColumn(nullable=false)
     */
    private $guideline;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="guidelineTable")
     */
    private $anchor;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->anchor = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getGuideline(): ?Guideline
    {
        return $this->guideline;
    }

    public function setGuideline(?Guideline $guideline): self
    {
        $this->guideline = $guideline;

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchor(): Collection
    {
        return $this->anchor;
    }

    public function addAnchor(Anchor $anchor): self
    {
        if (!$this->anchor->contains($anchor)) {
            $this->anchor[] = $anchor;
            $anchor->setGuidelineTable($this);
        }

        return $this;
    }

    public function removeAnchor(Anchor $anchor): self
    {
        if ($this->anchor->contains($anchor)) {
            $this->anchor->removeElement($anchor);
            // set the owning side to null (unless already changed)
            if ($anchor->getGuidelineTable() === $this) {
                $anchor->setGuidelineTable(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
