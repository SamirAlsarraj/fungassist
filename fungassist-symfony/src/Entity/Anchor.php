<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnchorRepository")
 */
class Anchor
{
    const ANCHOR_LAYOUT=[
        'start'=> '=====start_anchor_id_start=====',
        'end'=>'=====end_anchor_id_end====='
    ];

    const ANCHOR_REVIEW_STATUS=[
        'OK'=> 'OK',
        'REJECTED' => 'REJECTED',
        'null'=> null,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $context;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EvidenceLevel", inversedBy="anchors")
     * @ORM\JoinColumn(nullable=true)
     */
    private $evidenceLevel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RecommendationLevel", inversedBy="anchors")
     * @ORM\JoinColumn(nullable=true)
     */
    private $recommendationLevel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Guideline", inversedBy="anchors")
     * @ORM\JoinColumn(nullable=false)
     */
    private $guideline;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\QuestionItem", inversedBy="anchors")
     * @ORM\JoinColumn(nullable=true)
     */
    private $questionItem;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GuidelineTable", inversedBy="anchor")
     */
    private $guidelineTable;

    /**
     * @ORM\ManyToOne(targetEntity="GuidelineFigure", inversedBy="anchor")
     */
    private $guidelineFigures;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Algorithm", inversedBy="anchorThen")
     * @ORM\JoinColumn(nullable=true)
     */
    private $algorithm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reviewStatus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BackendUser", inversedBy="reviewedAnchors")
     */
    private $reviewBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $reviewAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reviewCmt;

    /**
     * @ORM\ManyToMany(targetEntity="QuestionaireResult", mappedBy="anchorsDisplayed")
     */
    private $questionaireResults;

    public function __construct()
    {
        $this->questionaireResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = trim(str_replace("\xC2\xA0", ' ', html_entity_decode($text, null, 'UTF-8')));

        return $this;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): self
    {
        $this->context = trim(str_replace("\xC2\xA0", ' ', html_entity_decode($context, null, 'UTF-8')));

        return $this;
    }

    public function getEvidenceLevel(): ?EvidenceLevel
    {
        return $this->evidenceLevel;
    }

    public function setEvidenceLevel(?EvidenceLevel $evidenceLevel): self
    {
        $this->evidenceLevel = $evidenceLevel;

        return $this;
    }

    public function getRecommendationLevel(): ?RecommendationLevel
    {
        return $this->recommendationLevel;
    }

    public function setRecommendationLevel(?RecommendationLevel $recommendationLevel): self
    {
        $this->recommendationLevel = $recommendationLevel;

        return $this;
    }

    public function getGuideline(): ?Guideline
    {
        return $this->guideline;
    }

    public function setGuideline(?Guideline $guideline): self
    {
        $this->guideline = $guideline;

        return $this;
    }

    public function getQuestionItem(): ?QuestionItem
    {
        return $this->questionItem;
    }

    public function setQuestionItem(?QuestionItem $questionItem): self
    {
        $this->questionItem = $questionItem;

        return $this;
    }

    public function getGuidelineTable(): ?GuidelineTable
    {
        return $this->guidelineTable;
    }

    public function setGuidelineTable(?GuidelineTable $guidelineTable): self
    {
        $this->guidelineTable = $guidelineTable;

        return $this;
    }

    public function getGuidelineFigures(): ?GuidelineFigure
    {
        return $this->guidelineFigures;
    }

    public function setGuidelineFigures(?GuidelineFigure $guidelineFigures): self
    {
        $this->guidelineFigures = $guidelineFigures;

        return $this;
    }

    public function getAlgorithm(): ?Algorithm
    {
        return $this->algorithm;
    }

    public function setAlgorithm(?Algorithm $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    public function __toString()
    {
        return $this->getGuideline().': '.$this->getText();
    }

    public function getReviewStatus(): ?string
    {
        return $this->reviewStatus;
    }

    public function setReviewStatus(?string $reviewStatus): self
    {
        $this->reviewStatus = $reviewStatus;

        return $this;
    }

    public function getReviewBy(): ?BackendUser
    {
        return $this->reviewBy;
    }

    public function setReviewBy(?BackendUser $reviewBy): self
    {
        $this->reviewBy = $reviewBy;

        return $this;
    }

    public function getReviewAt(): ?\DateTimeInterface
    {
        return $this->reviewAt;
    }

    public function setReviewAt(?\DateTimeInterface $reviewAt): self
    {
        $this->reviewAt = $reviewAt;

        return $this;
    }

    public function getReviewCmt(): ?string
    {
        return $this->reviewCmt;
    }

    public function setReviewCmt(?string $reviewCmt): self
    {
        $this->reviewCmt = $reviewCmt;

        return $this;
    }

    /**
     * @return Collection|QuestionaireResult[]
     */
    public function getquestionaireResult(): Collection
    {
        return $this->questionaireResults;
    }

    public function addquestionaireResult(QuestionaireResult $questionaireResults): self
    {
        if (!$this->questionaireResults->contains($questionaireResults)) {
            $this->questionaireResults[] = $questionaireResults;
            $questionaireResults->addAnchorsDisplayed($this);
        }

        return $this;
    }

    public function removequestionaireResult(QuestionaireResult $questionaireResults): self
    {
        if ($this->questionaireResults->contains($questionaireResults)) {
            $this->questionaireResults->removeElement($questionaireResults);
            $questionaireResults->removeAnchorsDisplayed($this);
        }

        return $this;
    }
}
